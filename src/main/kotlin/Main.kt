fun main() {

    val rame = Point(1,2)
    val rume = Point(1,2)

//    println(rame == rume)
//    println(rame.equals(rume))
//
//    println(rame.toString())
    println(rame.aigebiancaigebian(2))

    val f1 = Fraction(1,2)
    val f2 = Fraction(3,7)

    println(f1.Shekreba(f2))

    println(f1.gamravleba(f2))

    println(f1.shekveca())




}

class Point(x: Int, y: Int) {

    private var abscisa: Int = x
    private var ordinata: Int = y

    init {
        abscisa = x
        ordinata = y
    }

    override fun equals(other: Any?): Boolean {
        if (other is Point) {
            if ((this.abscisa == other.abscisa) and (this.ordinata == other.ordinata))
                return true
        }
        return false
    }

    override fun toString(): String {
        return "${this.abscisa} | ${this.ordinata}"
    }

    fun aigebiancaigebian(mandzili: Int): String {

        val newx: Int = abscisa + mandzili
        val newy: Int = ordinata + mandzili

        return "${newx} | ${newy}"

    }
}

class Fraction(n: Int, d: Int) {

    var numerator: Int = n
    var denominator: Int = d

    init {
        numerator = n
        denominator = d
    }

    override fun equals(other: Any?): Boolean {
        if (other is Fraction){
            if (numerator * other.denominator == denominator * other.numerator){
                return true
            }
        }
        return false
    }

    override fun toString(): String {
        return "${numerator} / ${denominator}"
    }

    fun Shekreba(r: Fraction): String {

        if (r.denominator == this.denominator){

            val newnumerator: Int = this.numerator + r.numerator
            val newdenominator: Int = this.denominator + r.denominator

            return "${newnumerator} / ${newdenominator}"
        }
        else {
            val saerto_jeradi: Int = r.denominator * this.denominator
            val out: Int = (this.numerator * r.denominator) + (r.numerator * this.denominator)

            return "${out} / ${saerto_jeradi}"

        }

    }

    fun gamravleba(g: Fraction): String {

        if (this.numerator == g.numerator) {
            val eeee: Int = this.denominator * g.denominator
            return "${this.numerator} / ${eeee}"
        }
        else {
            val ae: Int = this.numerator * g.numerator
            val oe: Int = this.denominator * g.denominator

            return "${ae} / ${oe}"
        }

    }

    fun shekveca(): String {

        if (denominator % numerator == 0) {

            val nnum: Int = numerator / numerator
            val ndeno: Int = denominator / numerator

            return "${nnum} / ${ndeno}"

        }
        else {
            return "Mocemuli wiladi ar ikveceba!"
        }
    }

}